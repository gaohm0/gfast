module github.com/tiger1103/gfast/v3

go 1.16

require (
	github.com/casbin/casbin/v2 v2.42.0
	github.com/go-sql-driver/mysql v1.7.0 // indirect
	github.com/gogf/gf/contrib/drivers/mysql/v2 v2.2.5
	github.com/gogf/gf/contrib/nosql/redis/v2 v2.3.0
	github.com/gogf/gf/v2 v2.3.1
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/mojocn/base64Captcha v1.3.5
	github.com/mssola/user_agent v0.5.3
	github.com/qiniu/go-sdk/v7 v7.13.0
	github.com/rivo/uniseg v0.4.3 // indirect
	github.com/shirou/gopsutil v3.21.11+incompatible
	github.com/tencentyun/cos-go-sdk-v5 v0.7.34
	github.com/tiger1103/gfast-cache v1.0.0
	github.com/tiger1103/gfast-token v1.0.3
	github.com/tklauser/go-sysconf v0.3.10 // indirect
	github.com/yusufpapurcu/wmi v1.2.2 // indirect
	golang.org/x/net v0.4.0 // indirect
)
